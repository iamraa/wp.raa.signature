<div class="wrap">
    <h2>RAA: подпись</h2>

    <p><b>Пример:</b></p>
    <p>[raaSignature] или [raaSignature token="main"]</p>

    <form method="post" action="options.php">
		<?php wp_nonce_field( 'update-options' ); ?>
		<?php settings_fields( 'raa-signature' ); ?>
		<?php $settings_user_id = RaaSignature::getUserId(); ?>

        <table class="form-table">

            <tr valign="top">
                <th scope="row">Показывать подписи:</th>
                <td><input type="checkbox" name="raa-signature[<?= $settings_user_id ?>][is_visible]"
                           value="1" <?= ! empty( RaaSignature::$options[ $settings_user_id ]['is_visible'] )
						? 'checked' : ''; ?>/></td>
            </tr>

            <tr valign="top">
                <th scope="row">CSS-стили блока:</th>
                <td><input type="text" name="raa-signature[<?= $settings_user_id ?>][css_style]"
                           value="<?= RaaSignature::$options[ $settings_user_id ]['css_style']; ?>" style="width:100%"/>
                </td>
            </tr>

            <tr valign="top">
                <th scope="row">Подпись MAIN:</th>
                <td><textarea cols="50" rows="10" name="raa-signature[<?= $settings_user_id ?>][main]"
                              style="width:100%"><?= RaaSignature::$options[ $settings_user_id ]['main']; ?></textarea>
                </td>
            </tr>

        </table>

        <?= submit_button(); ?>

    </form>
</div>
