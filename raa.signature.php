<?php
/*
Plugin Name: RAA.Signature
Description: Add shortcode with signature
Version: 0.1
License: GPL2

*/

/**
 * Class RaaSignature
 *
 * Add signature to posts
 * Example: [raaSignature token="main"]
 */
class RaaSignature {
	public static $options = [];
	public static $options_default = [
		'is_visible' => '0',
		'main'       => '',
		'css_style'  => '',
	];
	static $_styleIncluded = false;

	/**
	 * Signature
	 *
	 * @param array $attr
	 *
	 * @return string
	 */
	public static function shortcodeSignature( $attr = [], $content = "" ) {
		$attrs = shortcode_atts(
			[
				'token' => 'main',
			], $attr
		);

		self::getOptions();
		$user_id = self::getUserId();
		$options = self::$options_default;
		if ( ! empty( self::$options[ $user_id ] ) ) {
			$options = self::$options[ $user_id ];
		}

		//var_dump( self::$options, $attrs, $content );

		if ( empty( $attrs['token'] ) || empty( $options['is_visible'] ) ) {
			return '';
		}

		// add signature
		if ( ! empty( $options[ $attrs['token'] ] ) ) {
			$content = $options[ $attrs['token'] ];
		} else {
			$content = $options['main'];
		}

		$html =
			'<div class="raa-signature" style="' . $options['css_style'] . '">' . $content . '</div>';

		// Add styles
		/*if ( ! self::$_styleIncluded ) {
			add_action( 'wp_footer', [ 'RaaSignature', 'footer' ] );
			self::$_styleIncluded = true;
		}*/

		return $html;

	}

	protected static function getOptions() {
		$options = get_option( 'raa-signature' );
		if ( ! empty( $options ) ) {
			self::$options = $options;
		}

		$user_id = self::getUserId();
		if ( empty( self::$options[ $user_id ] ) ) {
			self::$options[ $user_id ] = self::$options_default;
		}

	}

	static function getUserId() {
		$user_id = get_current_user_id();
		if ( $post = get_post() ) {
			$user_id = $post->post_author;
		}

		return $user_id;
	}

	public static function footer() {
		?>
        <style rel="stylesheet" type="text/css">
            .raa-signature { padding: 10px 0; }
        </style>
		<?php
	}

	public static function activate() {
		add_option( 'raa-signature', self::$options );
	}

	public static function deactive() {
		delete_option( 'raa-signature' );
	}

	public static function adminInit() {
		register_setting( 'raa-signature', 'raa-signature' );

	}

	public static function adminMenu() {
		add_options_page(
			'RAA.Signature', 'RAA.Signature',
			'manage_options', 'RaaSignature',
			[ 'RaaSignature', 'optionsPage' ]
		);

	}

	public static function optionsPage() {
		self::getOptions();
		include dirname( __FILE__ ) . '/options.php';

	}

	public static function filterUpdate( $value, $option ) {
		if ($option == 'raa-signature') {
			$oldOptions = get_option( 'raa-signature' );
			// Leave other users' settings before saving
			foreach ($oldOptions as $user_id => $settings) {
				if (empty($value[$user_id])) {
					$value[$user_id] = $settings;
				}
			}
			//var_dump( $oldOptions, $value, $option );
			//die();
		}
		return $value;
	}

}

register_activation_hook( __FILE__, [ 'RaaSignature', 'activate' ] );
register_deactivation_hook( __FILE__, [ 'RaaSignature', 'deactive' ] );

if ( is_admin() ) {
	add_action( 'admin_init', [ 'RaaSignature', 'adminInit' ] );
	add_action( 'admin_menu', [ 'RaaSignature', 'adminMenu' ] );
	add_filter( 'pre_update_option', [ 'RaaSignature', 'filterUpdate' ], 10, 2 );
} else {
	add_shortcode( 'raaSignature', [ 'RaaSignature', 'shortcodeSignature' ] );
}